package com.soul.mode.command;

/**
 * 命令
 */
public abstract class Command {

    Receiver receiver;

    public Command(Receiver receiver) {
        this.receiver = receiver;
    }

    public abstract void exec();
}
