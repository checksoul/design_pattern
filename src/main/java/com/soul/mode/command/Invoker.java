package com.soul.mode.command;

/**
 * 命令执行者
 */
public class Invoker {

    Command command;

    public void setCommand(Command command) {
        this.command = command;
    }

    public void execute(){
        command.exec();
    }
}
