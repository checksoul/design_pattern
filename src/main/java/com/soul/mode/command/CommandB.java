package com.soul.mode.command;

/**
 * 命令b
 */
public class CommandB extends Command {

    public CommandB(Receiver receiver) {
        super(receiver);
    }

    @Override
    public void exec() {
        System.out.print("B  ");
        receiver.receiver();
    }
}
