package com.soul.mode.command;

/**
 * 命令模式
 * 将一个请求封装成一个对象，从而使得对不同的请求对客户进行参数化，对请求排队或者记录请求日志，以及支持撤销操作
 */
public class CommandTest {
    public static void main(String[] args){

        Receiver receiver = new Receiver();
        Command commandA = new CommandA(receiver);
        Command commandB = new CommandB(receiver);

        Invoker invoker = new Invoker();
        invoker.setCommand(commandA);
        invoker.execute();

        invoker.setCommand(commandB);
        invoker.execute();
    }
}
