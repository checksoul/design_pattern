package com.soul.mode.command;

/**
 * 命令a
 */
public class CommandA extends Command {

    public CommandA(Receiver receiver) {
        super(receiver);
    }

    @Override
    public void exec() {
        System.out.print("A  ");
        receiver.receiver();
    }
}
