package com.soul.mode.factory.factory_method;

/**
 * 字符串操作实现
 */
public class StringOperation implements Operation {
    @Override
    public void operat() {
        System.out.println("String opteration");
    }
}
