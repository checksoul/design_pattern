package com.soul.mode.factory.factory_method;

/**
 * 字符串操作创建工厂
 */
public class StringFactory implements IFactory {
    @Override
    public Operation create() {
        return new StringOperation();
    }
}
