package com.soul.mode.factory.factory_method;

/**
 * 整型操作实现
 */
public class IntegerOperation implements Operation {
    @Override
    public void operat() {
        System.out.println("int operation");
    }
}
