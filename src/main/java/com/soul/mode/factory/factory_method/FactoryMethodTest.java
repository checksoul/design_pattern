package com.soul.mode.factory.factory_method;

/**
 * 工厂方法模式
 * 定义一个用户创建对象的接口，让子类决定实例化哪一个类，工厂方法使一个雷的实例化延迟到其子类
 */
public class FactoryMethodTest {
    public static void main(String[] args){
        IFactory factory = new StringFactory();
        Operation operation = factory.create();
        operation.operat();
    }
}
