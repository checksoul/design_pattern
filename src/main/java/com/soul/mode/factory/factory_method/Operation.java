package com.soul.mode.factory.factory_method;

/**
 * 工厂生产类的接口
 */
public interface Operation {
    void operat();
}
