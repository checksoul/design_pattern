package com.soul.mode.factory.factory_method;

/**
 * 工厂接口，具体工厂类要实现此接口
 */
public interface IFactory {
    Operation create();
}
