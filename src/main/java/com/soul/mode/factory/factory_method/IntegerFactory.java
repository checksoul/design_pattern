package com.soul.mode.factory.factory_method;

/**
 * 整型操作创建工厂
 */
public class IntegerFactory implements IFactory {
    @Override
    public Operation create() {
        return new IntegerOperation();
    }
}
