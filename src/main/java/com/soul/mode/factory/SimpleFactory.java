package com.soul.mode.factory;

/**
 * 简单工厂模式
 * 工厂类包含必要的判断逻辑，更具客户端的选择动态创建相关的类
 *
 * ----如果有其他需求，需要修改工厂方法，违背了open-colsed原则
 */
public class SimpleFactory {

    private SimpleFactory(){}

    private static SimpleFactory simpleFactory = new SimpleFactory();

    public static SimpleFactory getInstance(){
        return simpleFactory;
    }

    public <T> T get(String key){
        if (key == null){
            throw new IllegalArgumentException("key is null");
        }
        switch (key){
            case "string":return (T) new String();
            case "int":return (T) new Integer(0);
            default:throw new IllegalArgumentException("key is illegal");
        }
    }

    public static void main(String[] args){
        System.out.println(getInstance().get("int").getClass().getSimpleName());
    }
}
