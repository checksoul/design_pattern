package com.soul.mode.chain_of_responsibility;

/**
 * 处理器链
 */
public class Handler3 extends Handler {
    @Override
    public void doReq() {
        System.out.println("handler 3");
        if (handler != null) {
            handler.doReq();
        }
        System.out.println("handler 3 end");
    }
}
