package com.soul.mode.chain_of_responsibility;

/**
 * 处理请求的接口
 */
public abstract class Handler {

    Handler handler;

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public abstract void doReq();
}
