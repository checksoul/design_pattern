package com.soul.mode.chain_of_responsibility;

/**
 * 职责链模式
 * 使得多个对象都有机会处理请求，从而避免请求的发送者和接收者之间的耦合关系，将这个对象连接成一条链，并沿着这条链传递下去，知道有一个对象处理为止
 */
public class ChainTest {

    public static void main(String[] args){
        //创建每个处理器
        Handler handler1 = new Handler1();
        Handler handler2 = new Handler2();
        Handler handler3 = new Handler3();
        //组织处理器链
        handler1.setHandler(handler2);
        handler2.setHandler(handler3);

        //执行处理器链
        handler1.doReq();
    }
}
