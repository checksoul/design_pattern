package com.soul.mode.prototype;


/**
 * 原型对象
 * 一般在初始化信息不发生变化的情况下，克隆是最好的办法
 */
public class Prototype implements Cloneable {

    private String name;
    private String addr;
    private Work work;

    public Prototype() {
        this.work = new Work();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public Work getWork() {
        return work;
    }

    public void setWork(String name) {
        this.work.setName(name);
    }

    @Override
    protected Prototype clone() throws CloneNotSupportedException {
        //当对象中含有可变的引用类型属性时，在复制得到的新对象对该引用类型属性内容进行修改，原始对象响应的属性内容也会发生变化，这就是"浅拷贝"的现象。
        //return (Prototype) super.clone();

        Prototype clone = (Prototype) super.clone();
        clone.work = work.clone();
        return clone;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Prototype{");
        sb.append("name='").append(name).append('\'');
        sb.append(", addr='").append(addr).append('\'');
        sb.append(", work=").append(work);
        sb.append('}');
        return sb.toString();
    }
}
