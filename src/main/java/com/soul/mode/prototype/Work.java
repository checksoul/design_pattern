package com.soul.mode.prototype;

/**
 * 原型中引用类型的属性
 */
public class Work implements Cloneable {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    protected Work clone() throws CloneNotSupportedException {
        return (Work) super.clone();
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Work{");
        sb.append("name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
