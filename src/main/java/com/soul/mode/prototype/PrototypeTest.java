package com.soul.mode.prototype;


/**
 * 原型模式
 * 用原型对象指定创建对象的种类，并且通过拷贝这些原型创建新的对象
 */
public class PrototypeTest {
    public static void main(String[] args) throws CloneNotSupportedException {
        Prototype prototype = new Prototype();
        prototype.setAddr("beijing");
        prototype.setWork("code farmer 1");
        prototype.setName("1");

        Prototype prototype1 = prototype.clone();
        prototype1.setName("2");
        prototype1.setWork("code farmer 2");
        Prototype prototype2 = prototype.clone();
        prototype2.setName("3");
        prototype2.setWork("code farmer 3");

        System.out.println(prototype + "|" + prototype1 + "|" + prototype2);
    }
}
