package com.soul.mode.strategy;

/**
 * 策略上下文
 */
public class StrategyContext {

    private SimpleStrategy simpleStrategy;

    /**
     * 策略工厂模式
     * @param strategy 策略名称
     */
    public StrategyContext(String strategy){
        switch (strategy){
            case "A":simpleStrategy = new StrategyA();break;
            case "B":simpleStrategy = new StrategyB();break;
            default:simpleStrategy = new StrategyA();//默认策略
        }
    }

    /**
     * 普通策略模式
     * @param simpleStrategy 策略对象
     */
    public StrategyContext(SimpleStrategy simpleStrategy){
        this.simpleStrategy = simpleStrategy;
    }

    /**
     * 执行策略方法
     * @return 策略执行结果
     */
    public Object result(){
        return simpleStrategy.get();
    }

    public static void main(String[] args){
        StrategyContext context = new StrategyContext("A");
        System.out.println(context.result());
    }
}
