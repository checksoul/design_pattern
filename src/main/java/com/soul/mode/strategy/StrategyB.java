package com.soul.mode.strategy;

/**
 * B策略
 */
public class StrategyB implements SimpleStrategy {
    @Override
    public Object get() {
        return "B strategy";
    }
}
