package com.soul.mode.strategy;

/**
 * A策略
 */
public class StrategyA implements SimpleStrategy {
    @Override
    public Object get() {
        return "A strategy";
    }
}
