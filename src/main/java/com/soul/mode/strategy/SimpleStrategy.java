package com.soul.mode.strategy;

/**
 * 简单策略模式
 */
public interface SimpleStrategy {
    Object get();
}
