package com.soul.mode.singleton;

/**
 * 第一种单例模式
 */
public final class SingletonOne {

    private SingletonOne(){}

    private static SingletonOne singletonOne = new SingletonOne();

    public synchronized static SingletonOne getInstance(){
        return singletonOne;
    }
}
