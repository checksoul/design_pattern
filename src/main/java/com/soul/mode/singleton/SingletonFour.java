package com.soul.mode.singleton;

/**
 * 第四种单例模式
 */
public enum SingletonFour {
    INSTANCE;
    private String name;
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }

    public static final SingletonFour getInstance(){
        return INSTANCE;
    }
}
