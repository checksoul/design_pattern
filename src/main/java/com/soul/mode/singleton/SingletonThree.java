package com.soul.mode.singleton;

/**
 * 第三种单例模式
 */
public final class SingletonThree {

    private static class Singleton{
        private static final SingletonThree instance = new SingletonThree();
    }
    private SingletonThree(){}

    public static SingletonThree getInstance(){
        return Singleton.instance;
    }
}
