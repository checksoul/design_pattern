package com.soul.mode.singleton;

/**
 * 第二种单例模式
 */
public final class SingletonTwo {

    private SingletonTwo(){}

    private static volatile SingletonTwo singletonTwo;

    public static SingletonTwo getInstance(){
        if (singletonTwo == null){
            synchronized (SingletonTwo.class){
                if (singletonTwo == null){
                    singletonTwo = new SingletonTwo();
                }
            }
        }
        return singletonTwo;
    }
}
