package com.soul.mode.bridge;

/**
 * 桥接模式
 * 将base 与 bridge 之间进行桥接
 */
public class BridgeTest {

    public static void main(String[] args){
        Bridge bridge = new Bridge();
        bridge.setBase(new BaseA());
        bridge.operation();

        bridge.setBase(new BaseB());
        bridge.operation();
    }
}
