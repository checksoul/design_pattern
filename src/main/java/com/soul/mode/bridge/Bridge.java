package com.soul.mode.bridge;


public class Bridge {

    Base base;

    public void setBase(Base base) {
        this.base = base;
    }

    public void operation(){
        base.run();
    }
}
