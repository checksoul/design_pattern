package com.soul.mode.viewer;

/**
 * 主题（发布者，通知类）接口
 */
public interface Subject {
    void add(Observer observer);
    void delete(Observer observer);
    void publish();
}
