package com.soul.mode.viewer.entrust;

/**
 * 用户（订阅者）
 */
public class Observer2 {

    private String name;

    public Observer2(String name) {
        this.name = name;
    }

    public void update() {
        System.out.println(name + " 被更新了 ");
    }
}