package com.soul.mode.viewer.entrust;

/**
 * 观察者模式（发布订阅模式）
 * 观察者模式主要是为了解除耦合，让耦合的的双方都依赖于抽象，而不是依赖于具体
 */
public class ViewerTest {
    public static void main(String[] args){
        Subject subject = new SubjectInfo();

        Observer1 observer1 = new Observer1("1");
        Observer2 observer2 = new Observer2("2");

        subject.publish(() -> observer1.close());

        subject.publish(() -> observer2.update());

    }
}
