package com.soul.mode.viewer.entrust;

import java.util.ArrayList;
import java.util.List;

/**
 * 具体发布者（主题）
 */
public class SubjectInfo implements Subject {

    @Override
    public void publish(EventHandler eventHandler) {
        eventHandler.update();
    }
}
