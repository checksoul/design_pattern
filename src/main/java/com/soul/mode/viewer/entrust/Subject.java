package com.soul.mode.viewer.entrust;

/**
 * 主题（发布者，通知类）接口
 */
public interface Subject {
    void publish(EventHandler eventHandler);
}
