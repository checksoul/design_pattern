package com.soul.mode.viewer.entrust;

/**
 * 用户（订阅者）
 */
public class Observer1 {

    private String name;

    public Observer1(String name) {
        this.name = name;
    }

    public void close() {
        System.out.println(name + " 被关闭了 ");
    }
}
