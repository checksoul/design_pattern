package com.soul.mode.viewer;

/**
 * 用户（订阅者）
 */
public class ObserverInfo implements Observer {

    private String name;

    public ObserverInfo(String name) {
        this.name = name;
    }

    @Override
    public void update() {
        System.out.println(name + " 被通知了 ");
    }
}
