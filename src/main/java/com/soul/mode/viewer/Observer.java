package com.soul.mode.viewer;

/**
 * 用户（订阅者）接口
 */
public interface Observer {
    void update();
}
