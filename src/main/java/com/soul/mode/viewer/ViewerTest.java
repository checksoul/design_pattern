package com.soul.mode.viewer;

/**
 * 观察者模式（发布订阅模式）
 * 观察者模式主要是为了解除耦合，让耦合的的双方都依赖于抽象，而不是依赖于具体
 */
public class ViewerTest {
    public static void main(String[] args){
        Subject subject = new SubjectInfo();

        Observer observer1 = new ObserverInfo("1");
        Observer observer2 = new ObserverInfo("2");

        subject.add(observer1);
        subject.add(observer2);

        subject.publish();

    }
}
