package com.soul.mode.viewer;

import java.util.ArrayList;
import java.util.List;

/**
 * 具体发布者（主题）
 */
public class SubjectInfo implements Subject {

    private List<Observer> observers = new ArrayList<>();

    @Override
    public void add(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void delete(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void publish() {
        for (Observer observer : observers) {
            observer.update();
        }
    }
}
