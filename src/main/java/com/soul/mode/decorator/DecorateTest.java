package com.soul.mode.decorator;

/**
 * 装饰模式测试
 */
public class DecorateTest {
    public static void main(String[] args){

        //原有对象，未被装饰
        Component component = new ConcreteComponent();

        //装饰器定义
        Decorator decorator1 = new DecoratorA();
        Decorator decorator2 = new DecoratorB();

        //开始装饰 DecoratorA
        decorator1.decorate(component);

        //开始装饰 DecoratorB，装饰已经装饰过的对象从而保证可多次装饰
        decorator2.decorate(decorator1);

        //装饰结果
        decorator2.show();

    }
}
