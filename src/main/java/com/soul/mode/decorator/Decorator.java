package com.soul.mode.decorator;

/**
 * 装饰类，用于装饰具体实现功能的类
 */
public abstract class Decorator implements Component {

    private Component component;

    @Override
    public void show() {
        if (component != null){
            component.show();
        }
    }

    public void decorate(Component component){
        this.component = component;
    }
}
