package com.soul.mode.decorator;

/**
 * 具体装饰类B，即装饰类的实现或者子类，用户特定的装饰功能
 */
public class DecoratorB extends Decorator {

    @Override
    public void show() {
        System.out.print("B  ");
        super.show();
    }
}
