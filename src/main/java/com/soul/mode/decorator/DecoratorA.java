package com.soul.mode.decorator;

/**
 * 具体装饰类A，即装饰类的实现或者子类，用户特定的装饰功能
 */
public class DecoratorA extends Decorator {

    @Override
    public void show() {
        System.out.print("A  ");
        super.show();
    }
}
