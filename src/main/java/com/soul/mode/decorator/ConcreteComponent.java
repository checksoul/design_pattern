package com.soul.mode.decorator;

/**
 * 被装饰类的实现,即具体实现功能的类
 */
public class ConcreteComponent implements Component {

    @Override
    public void show() {
        System.out.println("Decorator");
    }
}
