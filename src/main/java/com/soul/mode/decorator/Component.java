package com.soul.mode.decorator;

/**
 * 被装饰类的接口，定义具体实现功能的接口
 */
public interface Component {
    void show();
}
