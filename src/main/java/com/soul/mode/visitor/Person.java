package com.soul.mode.visitor;

/**
 * 状态类接口，只能有有限的状态个数，如人分为男和女两个
 */
public interface Person {
    void accept(Visitor visitor);
}
