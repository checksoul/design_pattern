package com.soul.mode.visitor;

/**
 * 访问者接口
 */
public interface Visitor {
    void visita(Man man);
    void visitb(Woman woman);
}
