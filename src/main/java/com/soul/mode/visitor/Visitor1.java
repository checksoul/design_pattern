package com.soul.mode.visitor;

/**
 * 状态1
 */
public class Visitor1 implements Visitor {
    @Override
    public void visita(Man man) {
        System.out.println(man.getName() + ".....");
    }

    @Override
    public void visitb(Woman woman) {
        System.out.println(woman.getName() + ".....");
    }
}
