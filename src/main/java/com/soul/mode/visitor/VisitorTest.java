package com.soul.mode.visitor;

/**
 * 访问者模式
 * 表示一个作用于某个对象结构中的各元素的操作，它使你可以在不改变各元素的类的前提下定义作用于这些元素的新操作
 */
public class VisitorTest {
    public static void main(String[] args){
        Visitor visitor = new Visitor1();
        Man man = new Man();
        man.accept(visitor);

        Woman woman = new Woman();
        woman.accept(visitor);

        //新增一个操作，只需要添加一个Visitor子类，就可以实现不同状态
        Visitor visitor2 = new Visitor2();
        Man man2 = new Man();
        man2.accept(visitor2);

        Woman woman2 = new Woman();
        woman2.accept(visitor2);
    }
}
