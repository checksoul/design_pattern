package com.soul.mode.visitor;


public class Man implements Person {

    private String name = "man";

    public String getName() {
        return name;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visita(this);
    }
}
