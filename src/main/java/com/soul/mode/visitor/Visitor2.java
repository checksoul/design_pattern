package com.soul.mode.visitor;

/**
 * 状态2
 */
public class Visitor2 implements Visitor {
    @Override
    public void visita(Man man) {
        System.out.println(man.getName() + " 123");
    }

    @Override
    public void visitb(Woman woman) {
        System.out.println(woman.getName() + " 1234");
    }
}
