package com.soul.mode.visitor;


public class Woman implements Person {
    private String name = "woman";

    public String getName() {
        return name;
    }
    @Override
    public void accept(Visitor visitor) {
        visitor.visitb(this);
    }
}
