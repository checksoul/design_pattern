package com.soul.mode.adapter;

/**
 * 不需要被适配的类
 */
public class NormalRequest implements Request {
    @Override
    public void request() {
        System.out.println("normal");
    }
}
