package com.soul.mode.adapter;

/**
 * 适配器模式，将一个类的接口转换成客户端希望的另一个接口的
 * 是的原本不兼容的接口可以在一起工作
 */
public class AdapterTest {

    public static void main(String[] args){
        Request request = new NormalRequest();
        request.request();

        request = new Adapter();
        request.request();
    }
}
