package com.soul.mode.adapter;

/**
 * 适配OtherReequest
 */
public class Adapter implements Request {

    private OtherRequest otherRequest = new OtherRequest();

    @Override
    public void request() {
        otherRequest.otherRequest();
    }
}
