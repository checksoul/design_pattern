package com.soul.mode.template_method;

/**
 * 模板超类
 */
public abstract class Template {

    public void template(){
        String name = name();
        System.out.println(name + " say hello!");
    }

    protected abstract String name();

}
