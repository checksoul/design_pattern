package com.soul.mode.template_method;

/**
 * 子类A
 */
public class TemplateImplA extends Template {
    @Override
    protected String name() {
        return "A";
    }
}
