package com.soul.mode.template_method;

/**
 * 子类B
 */
public class TemplateImplB extends Template {
    @Override
    protected String name() {
        return "B";
    }
}
