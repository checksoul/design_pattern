package com.soul.mode.proxy;

/**
 * 代理类，需要实现共同接口
 * 为其他对象提供一种代理以控制对这个对象的访问
 */
public class Proxy implements Subject {

    //持有被代理对象
    private Subject subject;

    public Proxy() {
        this.subject = new RealSubject();
    }

    @Override
    public void request() {
        if (subject != null){
            System.out.println("代理");
            subject.request();
        }
    }

    public static void main(String[] args){
        Proxy proxy = new Proxy();
        proxy.request();
    }
}
