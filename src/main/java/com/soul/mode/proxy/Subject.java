package com.soul.mode.proxy;

/**
 * 代理和正式请求的共同接口
 */
public interface Subject {
    void request();
}
