package com.soul.mode.proxy.dynamic_proxy;

/**
 * 真实请求类，即被代理类
 */
public class RealSubject implements Subject {
    @Override
    public void request() {
        System.out.println("真实请求");
    }
}
