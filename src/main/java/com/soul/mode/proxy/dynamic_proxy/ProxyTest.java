package com.soul.mode.proxy.dynamic_proxy;

/**
 * 动态代理测试
 */
public class ProxyTest {

    public static void main(String[] args){
        //需要代理的类
        Subject subject = new RealSubject();
        //代理类
        Proxy proxy = new Proxy(subject);
        //创建代理类对象
        Subject subjectProxy = (Subject) java.lang.reflect.Proxy.newProxyInstance(subject.getClass().getClassLoader(),
                subject.getClass().getInterfaces(), proxy);
        //调用
        subjectProxy.request();
    }
}
