package com.soul.mode.proxy.dynamic_proxy;

/**
 * 代理和正式请求的共同接口
 */
public interface Subject {
    void request();
}
