package com.soul.mode.builder;

/**
 * 指挥者，用于建造产品
 */
public class Director {

    private Person person;

    public Director(Person person) {
        this.person = person;
    }

    public void builder(){
        person.run();
        person.say();
    }
}
