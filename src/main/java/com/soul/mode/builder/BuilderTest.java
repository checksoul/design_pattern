package com.soul.mode.builder;

/**
 * 建造者模式
 * 将一个复杂对象的构建与他的表示分离，使得构建过程可以创建不同的表示
 */
public class BuilderTest {
    public static void main(String[] args){

        Person person1 = new BuilderOne();
        Person person2 = new BuilderTwo();

        Director director1 = new Director(person1);
        Director director2 = new Director(person2);

        director1.builder();
        director2.builder();
    }
}
