package com.soul.mode.builder;

/**
 * 建造者模板
 */
public interface Person {
    void say();
    void run();
}
