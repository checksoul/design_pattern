package com.soul.mode.builder;

/**
 * 具体建造类two
 */
public class BuilderTwo implements Person {
    @Override
    public void say() {
        System.out.println("two say hello");
    }

    @Override
    public void run() {
        System.out.println("two run 200m");
    }
}