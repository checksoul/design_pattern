package com.soul.mode.builder;

/**
 * 具体建造类one
 */
public class BuilderOne implements Person {
    @Override
    public void say() {
        System.out.println("one say hello");
    }

    @Override
    public void run() {
        System.out.println("one run 100m");
    }
}
