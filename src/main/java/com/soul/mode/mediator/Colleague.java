package com.soul.mode.mediator;

/**
 * 同行抽象类
 */
public abstract class Colleague {

    Mediator mediator;

    public Colleague(Mediator mediator){
        this.mediator = mediator;
    }

    abstract void send(String str);

    abstract void notifymsg(String str);

}
