package com.soul.mode.mediator;

/**
 * Created on 2018/10/9.
 */
public class ColleagueImpl2 extends Colleague {
    public ColleagueImpl2(Mediator mediator) {
        super(mediator);
    }

    @Override
    public void send(String str) {
        mediator.send(str);
    }

    @Override
    public void notifymsg(String str) {
        System.out.println("2......." + str);
    }
}
