package com.soul.mode.mediator;

/**
 *
 */
public class ColleagueImpl1 extends Colleague {
    public ColleagueImpl1(Mediator mediator) {
        super(mediator);
    }

    @Override
    public void send(String str) {
        mediator.send(str);
    }

    @Override
    public void notifymsg(String str) {
        System.out.println("1......." + str);
    }
}
