package com.soul.mode.mediator;

/**
 * 中介接口类
 */
public interface Mediator {
    void send(String str);
}
