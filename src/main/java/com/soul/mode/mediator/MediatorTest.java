package com.soul.mode.mediator;

/**
 * 中介者模式
 * 用一个对象来封装一系列的对象交互，中介者使各对象不需要显示的相互引用，从而使其耦合松散，而且可以独立改变他们之间的交互
 */
public class MediatorTest {
    public static void main(String[] args){
        MediatorImpl mediator = new MediatorImpl();

        //同行与中介关联
        Colleague colleague1 = new ColleagueImpl1(mediator);
        Colleague colleague2 = new ColleagueImpl2(mediator);

        //中介关联同行
        mediator.setColleague1(colleague1);
        mediator.setColleague2(colleague2);

        //发送消息，通过中介
        colleague1.send("2");
        colleague2.send("1");
    }
}
