package com.soul.mode.mediator;

/**
 * 具体中介者
 */
public class MediatorImpl implements Mediator {

    private Colleague colleague1;
    private Colleague colleague2;

    public void setColleague1(Colleague colleague1) {
        this.colleague1 = colleague1;
    }

    public void setColleague2(Colleague colleague2) {
        this.colleague2 = colleague2;
    }

    @Override
    public void send(String str) {
        if (colleague1 != null && colleague2 != null){
            if ("1".equals(str)) {
                colleague2.notifymsg(str);
            }else {
                colleague1.notifymsg(str);
            }
        }
    }
}
