package com.soul.mode.flyweight;

import java.util.Hashtable;

/**
 * 享元工厂
 */
public class FlyweightFactory {
    private Hashtable hashtable = new Hashtable();

    public FlyweightFactory(){
        //共享对象，减少对象的创建
        hashtable.put("A",new ConcreteFlyweight());
        hashtable.put("B",new ConcreteFlyweight());
        hashtable.put("C",new ConcreteFlyweight());
    }

    public Flyweight get(String key){
        return (Flyweight) hashtable.get(key);
    }
}
