package com.soul.mode.flyweight;


public class UnshareFlyweight implements Flyweight {
    @Override
    public void operat() {
        System.out.println("UnshareFlyweight");
    }
}
