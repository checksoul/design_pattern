package com.soul.mode.flyweight;


public class ConcreteFlyweight implements Flyweight {
    @Override
    public void operat() {
        System.out.println("ConcreteFlyweight");
    }
}
