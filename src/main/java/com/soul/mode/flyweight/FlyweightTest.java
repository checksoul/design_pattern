package com.soul.mode.flyweight;

/**
 * 享元模式
 * 运用共享技术有效的支持大量细粒度的对象
 */
public class FlyweightTest {

    public static void main(String[] args){
        FlyweightFactory factory = new FlyweightFactory();
        Flyweight a = factory.get("A");
        a.operat();

        Flyweight b = factory.get("B");
        b.operat();

        Flyweight c = factory.get("C");
        c.operat();

        UnshareFlyweight flyweight = new UnshareFlyweight();
        flyweight.operat();
    }
}
