package com.soul.mode.facade;

/**
 * 外观模式
 * 为子系统的一组接口提供一个一直的界面，此模式定义了一个高层接口，这个接口使得这一子系统更加容易使用
 * 传统的mvc模式即是外观模式的最佳实践
 */
public class FacadeTest {
    public static void main(String[] args){
        Service service = new Service();
        System.out.println(service.operat());
    }
}
