package com.soul.mode.facade;

/**
 * 传统mvc模式中数据访问层,即外观模式中的子系统
 */
public class Dao2 {
    public String method(){
        return "2";
    }

}
