package com.soul.mode.facade;

/**
 * 外观模式提供外观的类，即传统mvc中的service层，封装对数据访问层的调用
 */
public class Service {

    private Dao1 dao1;
    private Dao2 dao2;

    public Service() {
        this.dao1 = new Dao1();
        this.dao2 = new Dao2();
    }

    public String operat(){
        return dao1.method() + " | " + dao2.method();
    }
}
