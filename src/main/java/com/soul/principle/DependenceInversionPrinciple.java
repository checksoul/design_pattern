package com.soul.principle;

/**
 * 依赖倒转原则
 * 遵循下面的规则：
 *      1.每个类尽量都有接口或者抽象类，或者抽象类和接口两都具备
 *      2.变量的表面类型尽量是接口或者抽象类
 *      3.任何类都不应该从具体类派生
 *      4.尽量不要覆写基类的方法，如果基类是一个抽象类，而这个方法已经实现了，子类尽量不要覆写。
 *      5.结合里氏替换原则使用
 */
public class DependenceInversionPrinciple {

    public String desc(){
        String str = "依赖倒转原则：A.高层模块不应该依赖底层模块，两个都应该依赖抽象\n " +
                "B.抽象不应该依赖细节，细节应该依赖抽象";
        return str;
    }
}
