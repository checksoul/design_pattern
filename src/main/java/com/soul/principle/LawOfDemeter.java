package com.soul.principle;

/**
 * 迪米特法则（最少知识原则）
 * 低耦合，高内聚
 */
public class LawOfDemeter {

    public String desc(){
        String str = "迪米特法则（最少知识原则）：如果两个类不必彼此直接通信，那么这两个类就不应当直接的相互作用，如果其中一个类需要调用另一个类的\n" +
                "某一个方法的话，可以通过第三者转发这个调用";
        return str;
    }
}
