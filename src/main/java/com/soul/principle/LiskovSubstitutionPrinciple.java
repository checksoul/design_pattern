package com.soul.principle;

/**
 * 里氏代换原则
 * 里氏替换原则包含以下4层含义：
 *      1.子类可以实现父类的抽象方法，但是不能覆盖父类的非抽象方法。
 *      2.子类中可以增加自己特有的方法。
 *      3.当子类覆盖或实现父类的方法时，方法的前置条件（即方法的形参）要比父类方法的输入参数更宽松。
 *      4.当子类的方法实现父类的抽象方法时，方法的后置条件（即方法的返回值）要比父类更严格。
 */
public class LiskovSubstitutionPrinciple {

    public String desc(){
        String str = "里氏代换原则：子类型必须能够替换掉它们的父类型";
        return str;
    }
}
