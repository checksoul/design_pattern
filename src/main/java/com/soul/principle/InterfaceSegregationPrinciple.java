package com.soul.principle;

/**
 * 接口隔离原则
 * 接口隔离原则与单一职责原则的审视角度不相同。单一职责原则要求是类和接口的职责单一，注重的提倡职责，这是业务逻辑上的划分。
 * 接口隔离原则要求接口的方法尽量少
 */
public class InterfaceSegregationPrinciple {

    public String desc(){
        String str = "接口隔离原则：客户端不应该依赖它不需要的接口；一个类对另一个类的依赖应该建立在最小的接口上。";
        return str;
    }
}
