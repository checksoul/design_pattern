package com.soul.principle;

/**
 * 单一职责原则（SRP）
 * 基本上类的单一职责都用了类似的一句话来说"This is sometimes hard to see"，这句话翻译过来
 * 就是“这个有时候很难说”。是的，类的单一职责确实受非常多因素的制约，纯理论地来讲，这个原则是
 * 非常优秀的，但是现实有现实的难处.
 */
public class SingleResponsibilityPrinciple {

    public String desc(){
        String str = "单一职责原则：就一个类而言，应该仅有一个引起它变化的原因";
        return str;
    }
}
