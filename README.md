## 设计模式示例

    适配器模式：adapter
    桥接模式：bridge
    建造者模式：builder
    职责链模式：chain_of_responsibility
    命令模式：command
    装饰模式：decorator
    外观模式：facade
    工厂模式：factory
    享元模式：flyweight
    中介者模式：mediator
    原型模式：prototype
    代理模式：proxy
    单例模式：singleton
    策略模式：strategy
    模板方法模式：template_method
    观察者模式：viewer
    访问者模式：visitor